# CarCar

Team:

- Nina Kapanadze - Sales
- Michael Kane. - Service

## Design

CarCar is a web appllication designed for car dealerships. Each application is split into its own microservice (Inventory, Sales, and Service) and utilizes RESTful API's - built with Django, React, and Docker.

Sales: ability to track car sales, add new sales, view sales record, add sales employees, and add potiential customers.
Service: ability to add a technician, schedule an appointment, search through service history by vin.

## Service microservice

Back-end:
I created 3 models: technician, appointment, and an automobileVO. The VO is the link back to the inventory microservice.

I added a few encoders to encoders.py so that data can be retrieved. These included a technician, appointment, and automobileVO encoders.

The views.py file handels all the required http methods for creating new appointments/technicians.
A 'vip' field is either true/false depending if the vehicle being serviced came from the dealerships inventory or not.

Front-end:
I used React with JSX and Bootstrap to build the front end of the application.

Integration:
The vip field that notes if the vehicle in questio, being serviced, came from the car dealership inventory.

## Sales microservice

For the sales microservice, the salesrecord model was built as a boundary around all the other models and primarily used to define the microservice.
