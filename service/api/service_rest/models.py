from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, blank=True)
    vin = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("", kwargs={"id": self.id})


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("technician", kwargs={"id": self.id})


class Appointment(models.Model):
    customer_name = models.CharField(max_length=100)
    date = models.CharField(max_length=15, null=True)
    time = models.CharField(max_length=10, null=True)
    reason = models.TextField()
    is_vip = models.BooleanField(default=False, null=True, blank=True)
    is_finished = models.BooleanField(default=False, null=True, blank=True)

    vin = models.CharField(max_length=20)

    technician = models.ForeignKey(
        Technician,
        related_name='technician',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.customer_name}"
