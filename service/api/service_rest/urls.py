from django.urls import path
from .views import api_list_technicians, api_detail_technician, api_list_appointments,  api_detail_appointment


urlpatterns = [
    path("technicians/", api_list_technicians, name="list_technicians"),
    path("technicians/<int:id>/", api_detail_technician, name="list_technicians"),
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", api_detail_appointment,
         name="detail_appointments"),
]
