import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Technician, Appointment
from .encoders import AutomobileVOEncoder, TechnicianEncoder, AppointmentEncoder


@require_http_methods(['GET', 'POST'])
def api_list_technicians(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Unable to create technician you silly goose."},
                status=400
            )


@require_http_methods(['GET', 'DELETE', 'PUT'])
def api_detail_technician(request, id):
    if request.method == 'GET':
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            {'technician': technician},
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == 'PUT':
        try:

            content = json.loads(request.body)
            Technician.objects.filter(id=id).update(**content)
            technician = Technician.objects.get(id=id)

            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "That person does not work here."},
                encoder=TechnicianEncoder,
                safe=False,
            )

    else:
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "That person does not work here."},
            )


@require_http_methods(['GET', 'POST'])
def api_list_appointments(request, automobile_vo_id=None):
    if request.method == 'GET':
        if automobile_vo_id is not None:
            appointments = Appointment.objects.filter(
                automobile=automobile_vo_id)
        else:
            appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )

    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(
                employee_number=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician identifier"},
                status=400
            )
        try:
            automobile = AutomobileVO.objects.get(vin=content["vin"])
            content["is_vip"] = True

        except AutomobileVO.DoesNotExist:
            pass

        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentEncoder,
            safe=False,
        )


@ require_http_methods(["DELETE", "PUT"])
def api_detail_appointment(request, id):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=400
            )

        return JsonResponse(
            {"message": "Appointment deleted"}
        )

    else:
        content = json.loads(request.body)
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)

        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
