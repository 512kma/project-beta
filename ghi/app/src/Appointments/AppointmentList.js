import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

const AppointmentList = (props) => {

  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {

    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);


  const handleDelete = async (e) => {

    const url = `http://localhost:8080/api/appointments/${e.target.id}/`;
    const fetchConfigs = {
      method: 'Delete',
      headers: {
        "Content-Type": "application/json",
      }
    };

    const response = await fetch(url, fetchConfigs);
    const data = await response.json();

    setAppointments(appointments.filter((appointment) => String(appointments.id) !== e.target.id));

    if (response.ok) {
      fetchData();
    }

  };

  return (
    <>
      <div className="my-5 row">
        <div className="offset-1 col-10">
          <div className="shadow p-4 mt-4">
            <h1>Appointments</h1>
            <h2>
              <Link to="new" className="btn btn-success">
                Create an Appointment
              </Link>
            </h2>

            <table className="table table-hover">
              <thead>
                <tr className='text-center'>
                  <th>Vin</th>
                  <th>Name</th>
                  <th>VIP status</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {appointments && appointments.map(appointment => {

                  return (
                    <tr key={appointment.id}>
                      <td>{appointment.vin}</td>
                      <td>{appointment.customer_name}</td>
                      <td>{String(appointment.is_vip)}</td>
                      <td>{appointment.date}</td>
                      <td>{appointment.time}</td>
                      <td>{appointment.technician.name}</td>
                      <td>{appointment.reason}</td>
                      <td><button onClick={handleDelete} id={appointment.id} className="btn btn-danger">Cancel</button><button onClick={handleDelete} id={appointment.id} className="btn btn-success">Finished</button></td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default AppointmentList;
