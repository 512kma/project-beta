import React, { useEffect, useState } from 'react';

const ServiceHistory = () => {

  const [appointment, setAppointment] = useState([]);
  const [filterTerm, setFilterTerm] = useState('');

  const getData = async () => {

    const response = await fetch("http://localhost:8080/api/appointments/");

    if (response.ok) {
      const data = await response.json();

      const appointment = data.appointments.map((appoint) => {
        return {
          vin: appoint.vin,
          customerName: appoint.customer_name,
          vip: appoint.is_vip,
          date: appoint.date,
          time: appoint.time,
          technician: appoint.technician.name,
          reason: appoint.reason,
        };
      });
      setAppointment(appointment);
    };
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterTerm(e.target.value);
  };


  return (
    <>
      <div className="my-5 row">
        <div className="offset-1 col-10">
          <div className="shadow p-4 mt-4">
            <h1>Service History</h1>
            <div className="form-group">
              <label htmlFor="search">Search by VIN:</label>
              <input type="text" className="form-control" onChange={handleFilterChange} id="search" />
            </div>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>Vin</th>
                  <th>Name</th>
                  <th>VIP status</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                </tr>
              </thead>
              <tbody>
                {appointment
                  .filter((appoint) => appoint.vin === filterTerm)
                  .map((appoint) => {
                    return (
                      <tr key={appoint.vin}>
                        <td>{appoint.vin}</td>
                        <td>{appoint.customerName}</td>
                        <td>{String(appoint.is_vip)}</td>
                        <td>{appoint.date}</td>
                        <td>{appoint.time}</td>
                        <td>{appoint.technician}</td>
                        <td>{appoint.reason}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default ServiceHistory;
