import React, { useEffect, useState } from 'react';

function SalesForm() {
  const [autos, setAutos] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [formData, setFormData] = useState({
    automobile: '',
    salesperson_id: '',
    customer_id: '',
    price: '',
  });


  const getData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
    }
  };

  const getData1 = async () => {
    const url1 = 'http://localhost:8090/api/salesperson/';
    const response1 = await fetch(url1);

    if(response1.ok) {
      const data1 = await response1.json();
      setSalespeople(data1.salespeople);
    }
  };

  const getData2 = async () => {
    const url2 = 'http://localhost:8090/api/customers/';
    const response2 = await fetch(url2);

    if(response2.ok) {
      const data2 = await response2.json();
      setCustomers(data2.customers);
    }
  };

  useEffect(() => {
    getData(); getData1(); getData2();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const salesrecordsUrl = 'http://localhost:8090/api/salesrecords/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    };
    const response = await fetch(salesrecordsUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        automobile: '',
        salesperson_id: '',
        customer_id: '',
        price: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-sales-form">
            <div className='mb-3'>
              <select onChange={handleFormChange} value={formData.automobile.href} required name='automobile' id='automobile' className='form-select'>
                <option value=''>Choose an automobile</option>
                {autos.map(auto => {
                  return (
                    <option key={auto.href} value={auto.id}>
                      {auto.href}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className='mb-3'>
              <select onChange={handleFormChange} value={formData.salesperson_id} required name='salesperson_id' id='salesperson_id' className='form-select'>
                <option value=''>Choose a salesperson</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.name} value={salesperson.id}>
                      {salesperson.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className='mb-3'>
              <select onChange={handleFormChange} value={formData.customer_id} required name='customer_id' id='customer_id' className='form-select'>
                <option value=''>Choose a customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.name} value={customer.id}>
                      {customer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className='form-floating mb-3'>
              <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type='number' name='price' id='price' className='form-control' />
              <label htmlFor='price'>Sale Price</label>
            </div>
            <button className='btn btn-success'>Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;
