from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def get_api_url(self):
        return reverse("", kwargs={"id": self.id})


class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_show_salesperson", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField()
    phone_number = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"id": self.id})



class Salesrecord(models.Model):
    price = models.FloatField()

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesrecords",
        on_delete=models.CASCADE,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesrecords",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="salesrecords",
        on_delete=models.CASCADE,
    )
