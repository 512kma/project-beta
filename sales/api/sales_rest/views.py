from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Salesperson, Customer, AutomobileVO, Salesrecord
from .encoders import (
    SalespersonListEncoder,
    SalespersonDetailEncoder,
    CustomerListEncoder,
    CustomerDetailEncoder,
    SalesrecordListEncoder,
    SalesrecordDetailEncoder,
)


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salesperson}, encoder=SalespersonListEncoder
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson, encoder=SalespersonDetailEncoder, safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_salesperson(request, id):
    try:
        salesperson = Salesperson.objects.get(id=id)
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid salesperson id"},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            salesperson, encoder=SalespersonDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=id).update(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers}, encoder=CustomerListEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer, encoder=CustomerDetailEncoder, safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, id):
    try:
        customer = Customer.objects.get(id=id)
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid customer id"},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_salesrecords(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            salesrecords = Salesrecord.objects.filter(automobile=automobile_vo_id)
        else:
            salesrecords = Salesrecord.objects.all()
            return JsonResponse(
                {"salesrecords": salesrecords}, encoder=SalesrecordListEncoder, safe=False
            )
    else:
        content = json.loads(request.body)
        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=content["customer_id"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=404,
            )
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson_id"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )
        salesrecord = Salesrecord.objects.create(**content)
        return JsonResponse(
            salesrecord, encoder=SalesrecordDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_salesrecord(request, id):
    try:
        salesrecord = Salesrecord.objects.get(id=id)
    except Salesrecord.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid salesrecord id"},
            status=404,
        )
    if request.method == "GET":
        return JsonResponse(
            salesrecord,
            encoder=SalesrecordDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Salesrecord.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "automobile" in content:
                automobile = AutomobileVO.objects.get(
                    import_href=content["automobile"]
                )
                content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=404,
            )
        try:
            if "salesperson" in content:
                salesperson = Salesperson.objects.get(id=content["salesperson_id"])
                content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Invalid salesperson id"}, status=404)
        try:
            if "customer" in content:
                customer = Customer.objects.get(id=content["customer_id"])
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer id"}, status=404)
        Salesrecord.objects.filter(id=id).update(**content)
        return JsonResponse(
            salesrecord,
            encoder=SalesrecordDetailEncoder,
            safe=False
        )
